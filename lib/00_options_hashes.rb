# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
def transmogrify(some_string, options = {})
  copy_string = some_string.dup
  defaults = {
    times: 2,
    upcase: false,
    reverse: false
  }
  defaults.merge!(options)
  copy_string.upcase! if defaults[:upcase] == true
  copy_string.reverse! if defaults[:reverse] == true
  res = ""
  idx = 0
  while idx < defaults[:times]
     res += copy_string

     idx += 1
   end
   res
end
